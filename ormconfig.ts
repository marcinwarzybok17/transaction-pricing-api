export default {
    type: 'postgres',
    host: 'database',
    port: 5432,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    synchronize: true,
    logging: false,
    entities: ['src/infrastructure/model/**/*.ts'],
    migrations: ['src/infrastructure/migration/**/*.ts'],
    subscribers: ['src/infrastructure/subscriber/**/*.ts'],
    cli: {
        entitiesDir: 'src/infrastructure/model',
        migrationsDir: 'src/infrastructure/migration',
        subscribersDir: 'src/infrastructure/subscriber',
    },
}
