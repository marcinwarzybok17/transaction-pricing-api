import { DefaultPricingRule, MIN_TRANSACTION_FEE } from '../../src/infrastructure/service/DefaultPricingRule'

describe("DefaultPricingRule", () => {
    const defaultPricingRule = new DefaultPricingRule()

    it("should apply min fee", () => {
        expect(defaultPricingRule.apply(10)).toEqual(MIN_TRANSACTION_FEE)
    })

    it("should apply percentage fee", () => {
        expect(
            expect(defaultPricingRule.apply(200)).toEqual(1)
        )
    })
})