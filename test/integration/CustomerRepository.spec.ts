import { getConnection } from 'typeorm'
import { ICustomerRepository, CustomerRepository } from '../../src/infrastructure/repository/CustomerRepository'
import { IMain, Main } from '../../src/Main'
import { Customer } from '../../src/infrastructure/model/Customer'
import { Transaction } from '../../src/infrastructure/model/Transaction'

describe("CustomerRepository", () => {
    let main: IMain
    let customerRepository: ICustomerRepository

    beforeAll(async () => {
        main = new Main()
        await main.start()
        customerRepository = new CustomerRepository()
    })

    afterAll(async () => {
        await main.stop()
    })

    describe('getTransactionSumForCustomerInLast30Days', () => {
        it("should get proper sum of transactions in last 30 days", async () => {
            const customer = new Customer()

            const transaction = new Transaction()

            transaction.amount = 100
            transaction.transactionDate = new Date()

            const transaction2 = new Transaction()
            transaction2.amount = 200
            transaction2.transactionDate = new Date()

            const transaction3 = new Transaction()
            transaction3.amount = 200
            transaction3.transactionDate = new Date(1999, 1, 1)

            customer.transactions = [transaction, transaction2, transaction3]

            const createdCustomer = await getConnection().manager.save(customer)

            await
                expect(
                    customerRepository.getTransactionSumForCustomerInLast30Days(createdCustomer.id)
                ).resolves.toBe(300)

        })
    })

})