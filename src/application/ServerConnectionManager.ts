import { allowStaticMethods } from "../utils/allowStaticMethod";
import { ConnectionManager, ConnectionManagerStaticMethods } from "./ConnectionManager";
import { ExpressServer } from "./express-server/ExpressServer";
import { Server } from "./server/Server";
import { SERVER } from "./server/Servers";


@allowStaticMethods<ConnectionManagerStaticMethods>()
export class ServerConnectionManager implements ConnectionManager {

    serverConnections: Map<SERVER, Server> = new Map()
    private static instance: ServerConnectionManager

    static async createConnections(): Promise<ConnectionManager> {
        let serverConnectionManager = ServerConnectionManager.instance

        if (!serverConnectionManager) {
            serverConnectionManager = new ServerConnectionManager()
        }

        serverConnectionManager.serverConnections.set(
            SERVER.TRANSACTION_PRICING_API,
            new ExpressServer()
        )

        await serverConnectionManager.startAll()

        return serverConnectionManager
    }

    async startAll(): Promise<void> {
        await this.serverConnections.get(SERVER.TRANSACTION_PRICING_API).start(3000)
    }

    async stopAll(): Promise<void> {
        await this.serverConnections.get(SERVER.TRANSACTION_PRICING_API).close()
    }

}