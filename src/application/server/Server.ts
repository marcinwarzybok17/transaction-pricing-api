export interface Server {
    serverInstance: unknown
    start(port: number): Promise<void>
    close(): Promise<void>
    getServerInstance(): unknown
}

export interface ServerStaticMethods {
    runServer(port: number): Promise<Server> | Server
}
