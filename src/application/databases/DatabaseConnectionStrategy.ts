import { DATABASES } from './databases'
import { DATABASE_TYPES } from './DatabaseTypes'

export interface DatabaseConnectionStrategy {
    databaseName: DATABASES
    databaseConnection: unknown
    readonly databaseType: DATABASE_TYPES
    open(...args): unknown
    close(): unknown
}