import { allowStaticMethods } from '../../utils/allowStaticMethod'
import { ConnectionPooling, ConnectionPoolingStaticMethods } from './ConnectionPooling'
import { DatabaseConnectionStrategy } from './DatabaseConnectionStrategy'
import { DATABASES } from './databases'
import { PostgresConnectionStrategy } from './PostgresConnectionStrategy'

@allowStaticMethods<ConnectionPoolingStaticMethods>()
export class DatabaseConnectionPooling implements ConnectionPooling {
    private static connections: Map<
        DATABASES,
        DatabaseConnectionStrategy
    > = new Map()

    constructor() {
        DatabaseConnectionPooling.connections.set(
            DATABASES.TRANSACTIONS,
            new PostgresConnectionStrategy(DATABASES.TRANSACTIONS)
        )
    }

    static async openConnections(): Promise<DatabaseConnectionPooling> {
        const databaseConnectionPooling = new DatabaseConnectionPooling()
        await databaseConnectionPooling.openConnection(
            DATABASES.TRANSACTIONS
        )

        return databaseConnectionPooling
    }

    async openConnection<T>(databaseName: DATABASES, config?: T): Promise<void> {
        const connection =
            DatabaseConnectionPooling.getConnectionStrategy(databaseName)
        await connection.open(config)
    }

    async closeConnection(databaseName: DATABASES): Promise<void> {
        const connectionStrategy =
            DatabaseConnectionPooling.getConnectionStrategy(databaseName)
        await connectionStrategy.close()
        DatabaseConnectionPooling.connections.delete(databaseName)
    }

    async closeAllConnections(): Promise<void> {
        const databasesToCloseConnectionWith: DATABASES[] = []
        DatabaseConnectionPooling.connections.forEach(
            (_connection, key) => {
                databasesToCloseConnectionWith.push(key)
            }
        )
        const closeConnectionPromises = databasesToCloseConnectionWith.map(
            (databaseName) => this.closeConnection(databaseName)
        )
        await Promise.all(closeConnectionPromises)
    }

    static getConnectionStrategy(databaseName: DATABASES): DatabaseConnectionStrategy {
        const connectionStrategy =
            DatabaseConnectionPooling.connections.get(databaseName)

        if (!connectionStrategy) {
            throw new Error(
                `Missing database connection strategy at ${databaseName}`
            )
        }

        return connectionStrategy
    }

    static getConnection<T>(databaseName: DATABASES): T {
        const connectionStrategy =
            DatabaseConnectionPooling.getConnectionStrategy(databaseName)



        return connectionStrategy.databaseConnection as T
    }
}
