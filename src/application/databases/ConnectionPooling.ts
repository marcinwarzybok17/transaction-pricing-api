export interface ConnectionPooling {
    closeAllConnections(): Promise<void>
}

export interface ConnectionPoolingStaticMethods {
    openConnections(): Promise<ConnectionPooling>
    getConnection<T>(...args): T
}