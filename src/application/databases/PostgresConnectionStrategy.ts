import { Connection, createConnection } from 'typeorm'
import { DatabaseConnectionStrategy } from './DatabaseConnectionStrategy'
import { DATABASES } from './databases'
import { DATABASE_TYPES } from './DatabaseTypes'
import * as dotenv from 'dotenv'

dotenv.config()

export class PostgresConnectionStrategy implements DatabaseConnectionStrategy {
    databaseConnection: Connection
    databaseType = DATABASE_TYPES.POSTGRES
    databaseName: DATABASES

    constructor(databaseName: DATABASES) {
        this.databaseName = databaseName
    }

    async open(): Promise<void> {
        this.databaseConnection = await createConnection()
        this.databaseConnection.synchronize(true)
    }

    async close(): Promise<void> {
        if (!this.databaseConnection) {
            throw new Error('Cannot close not open connection')
        }
        await this.databaseConnection.close()
    }
}
