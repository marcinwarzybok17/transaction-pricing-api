import * as  express from 'express'
import { ITrasactionPricingService } from '../../infrastructure/service/TrasactionPricingService'
import { APIRoutes } from './ApiRoutes'

export class TransactionPricingApiRoutes implements APIRoutes {
    constructor(private app: express.Application, private transactionPricingService: ITrasactionPricingService) { }

    createRoutes(): void {
        this.app.post("/calculate-pricing-fee", async (req, res, next) => {
            try {
                const transaction = await this.transactionPricingService.getPricingForTransaction(
                    req.body.date,
                    req.body.currency,
                    Number(req.body.amount),
                    Number(req.body.client_id)
                )
                res.send({
                    amount: transaction.amount.toString(),
                    currency: transaction.currency
                })
            } catch (err) {
                next(err)
            }
        })
    }
}