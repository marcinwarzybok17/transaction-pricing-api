import { Server, ServerStaticMethods } from "../server/Server";
import * as express from 'express'
import { allowStaticMethods } from "../../utils/allowStaticMethod";
import { promisify } from "util";
import { Server as HttpServer } from 'http'
import { APIRoutes } from "./ApiRoutes";
import { TransactionPricingApiRoutes } from "./TransactionPricingApiRoutes";
import { TrasactionPricingService } from "../../infrastructure/service/TrasactionPricingService";
import { ExchangeService } from "../../infrastructure/service/ExchangeService";
import { CustomerRepository } from "../../infrastructure/repository/CustomerRepository";
import { HighTurnOverDiscountRule } from "../../infrastructure/service/HighTurnoverDiscountRule";
import { ExchangeRateApiRepository } from "../../infrastructure/repository/ExchangeRateApiRepository";
import { DomainError, DOMAIN_ERROR } from "../../domain/errors/DomainError";
import { InfrastructureError } from "../../infrastructure/errors/InfrastructureError";

export interface IExpressServer extends Server {
    serverInstance: HttpServer
    routeManager: APIRoutes
    start(port: number): Promise<void>
    close(): Promise<void>
    getServerInstance(): HttpServer
}

@allowStaticMethods<ServerStaticMethods>()
export class ExpressServer implements IExpressServer {
    routeManager: APIRoutes;
    serverInstance: HttpServer;

    static async runServer(port: number): Promise<Server> {
        const expressServer = new ExpressServer()
        await expressServer.start(port)
        return expressServer
    }

    async start(port: number): Promise<void> {
        const app = express()

        app.use(express.urlencoded({ extended: true }))
        app.use(express.json())

        const exchangeApiRepository = new ExchangeRateApiRepository()
        const exchangeService = new ExchangeService(exchangeApiRepository)
        const customerRepository = new CustomerRepository()
        const transactionPricingRule = new HighTurnOverDiscountRule()
        const transactionPricingService = new TrasactionPricingService(exchangeService, customerRepository, transactionPricingRule)
        this.routeManager = new TransactionPricingApiRoutes(app, transactionPricingService)
        this.routeManager.createRoutes()

        app.use((err, _req, res, _next) => {
            if (err instanceof DomainError) {
                return res.status(400).send({
                    msg: err.message
                })
            }

            if (err instanceof InfrastructureError) {
                return res.status(400).send({
                    msg: err.message
                })
            }

            res.status(500).send({})
        })

        app.listen(port, () => {
            console.log(`Server started on port ${port}`)
        })


    }

    async close(): Promise<void> {
        const close = promisify(this.serverInstance.close).bind(this.serverInstance)
        await close()
    }

    getServerInstance(): HttpServer {
        return this.serverInstance
    }

}