import { ConnectionManager } from "./application/ConnectionManager";
import { ConnectionPooling } from "./application/databases/ConnectionPooling";
import { DatabaseConnectionPooling } from "./application/databases/DatabaseConnectionPooling";
import { ServerConnectionManager } from "./application/ServerConnectionManager";

export interface IMain {
    start(): Promise<void>
    stop(): Promise<void>
}

export class Main implements IMain {

    serverConnectionManager: ConnectionManager
    databaseConnectionPooling: ConnectionPooling

    async start(): Promise<void> {
        this.databaseConnectionPooling = await DatabaseConnectionPooling.openConnections()
        this.serverConnectionManager = await ServerConnectionManager.createConnections()
    }

    async stop(): Promise<void> {
        if (!this.serverConnectionManager) {
            throw new Error("Server connections can't be close due missing initialization")
        }
        await this.serverConnectionManager.stopAll()
        await this.databaseConnectionPooling.closeAllConnections()
    }
}