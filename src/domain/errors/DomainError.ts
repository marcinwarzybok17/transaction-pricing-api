export enum DOMAIN_ERROR {
    VALIDATION_ERROR = 'VALIDATION_ERROR',
}

export class DomainError extends Error {
    domainError: DOMAIN_ERROR

    constructor(msg: string, domainError: DOMAIN_ERROR) {
        super(msg)
        Object.setPrototypeOf(this, DomainError.prototype)
        this.domainError = domainError
    }
}