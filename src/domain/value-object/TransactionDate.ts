import { DomainError, DOMAIN_ERROR } from "../errors/DomainError"

export class TransactionDate {
    private value: string

    constructor(value: string) {
        this.setValue = value
    }

    set setValue(value: string) {
        const YYYYMMDDRegexPattern = new RegExp(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)
        const isTransactionDateValid = YYYYMMDDRegexPattern.test(value)
        if (!isTransactionDateValid) {
            throw new DomainError(`Transaction date must be in yyyy-mm-dd format, got ${value}`, DOMAIN_ERROR.VALIDATION_ERROR)
        }

        this.value = value
    }

    get getValue(): string {
        return this.value
    }

}