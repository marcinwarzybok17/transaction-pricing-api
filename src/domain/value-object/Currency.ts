import { code } from 'currency-codes'
import { DomainError, DOMAIN_ERROR } from '../errors/DomainError';

export class Currency {
    private currencyCode: string;

    constructor(currencyCode: string) {
        this.setCurrencyCode = currencyCode
    }

    set setCurrencyCode(currencyCode: string) {
        const isValidCurrencyCode = !!code(currencyCode)

        if (!isValidCurrencyCode) {
            throw new DomainError(`Currency code is not valid ISO 4127 standard ${currencyCode}`, DOMAIN_ERROR.VALIDATION_ERROR)
        }

        this.currencyCode = currencyCode
    }

    get getCurrencyCode(): string {
        return this.currencyCode
    }

}