import { Currency } from "./Currency";

export class Money {
    private currency: Currency
    private amount: number;

    constructor(currency: Currency, amount: number) {
        this.currency = currency

        this.setAmount = amount
    }

    get getCurrency(): Currency {
        return this.currency
    }

    get getAmount(): number {
        return this.amount
    }

    set setAmount(amount: number) {
        this.amount = amount
    }
}