import { Currency } from "../value-object/Currency"
import { TransactionDate } from "../value-object/TransactionDate";
import { Customer } from "./Customer";

export class Transaction {
    private customer: Customer
    private date: TransactionDate
    private amount: number;
    private currency: Currency

    constructor(
        customer: Customer,
        date: TransactionDate,
        amount: number,
        currency: Currency
    ) {
        this.customer = customer
        this.currency = currency

        this.setDate = date
        this.setAmount = amount
    }

    set setDate(date: TransactionDate) {
        this.date = date
    }

    set setAmount(amount: number) {
        this.amount = amount
    }

    get getCustomer(): Customer {
        return this.customer
    }

    get getDate(): TransactionDate {
        return this.date
    }

    get getAmount(): number {
        return this.amount
    }

    get getCurrency(): Currency {
        return this.currency
    }

}