import { DomainError, DOMAIN_ERROR } from "../errors/DomainError"

export class Customer {
    constructor(private customerId: number) {
        this.setCustomerId = customerId
    }

    set setCustomerId(customerId: number) {
        if (typeof customerId !== "number") {
            throw new DomainError(`Customer id must be number, instead got ${typeof customerId}`, DOMAIN_ERROR.VALIDATION_ERROR)
        }
        this.customerId = customerId
    }

    get getCustomerId(): number {
        return this.customerId
    }
}