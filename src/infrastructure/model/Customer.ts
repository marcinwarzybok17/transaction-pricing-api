import { Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { Transaction } from './Transaction';

@Entity()
export class Customer {
    @PrimaryGeneratedColumn()
    id: number

    @OneToMany(() => Transaction, transaction => transaction.customer)
    transactions: Transaction[];
}
