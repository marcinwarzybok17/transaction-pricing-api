import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm'
import { Customer } from './Customer';

@Entity()
export class Transaction {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    amount: number

    @ManyToOne(() => Customer, customer => customer.transactions)
    customer: Customer;

    @Column()
    transactionDate: Date
}
