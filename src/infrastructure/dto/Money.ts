export interface MoneyDTO {
    amount: number;
    currency: string;
}