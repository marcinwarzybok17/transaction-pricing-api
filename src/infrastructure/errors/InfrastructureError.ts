export enum INFRASTRUCTURE_ERROR {
    INTERNAL_ERROR = 'INTERNAL_ERROR',
    VALIDATION_ERROR = 'VALIDATION_ERROR'
}

export class InfrastructureError extends Error {
    infrastructureError: INFRASTRUCTURE_ERROR

    constructor(infrastructureError: INFRASTRUCTURE_ERROR, msg?: string) {
        super(msg)
        Object.setPrototypeOf(this, InfrastructureError.prototype)
        this.infrastructureError = infrastructureError
    }
}
