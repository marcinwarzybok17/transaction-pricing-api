export interface DTOMapper<Entity, DTO> {
    fromEntityToDTO(entity: Entity): DTO
}