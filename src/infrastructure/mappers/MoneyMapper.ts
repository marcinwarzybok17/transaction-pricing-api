import { Money } from "../../domain/value-object/Money";
import { allowStaticMethods } from "../../utils/allowStaticMethod";
import { MoneyDTO } from "../dto/Money";
import { DTOMapper } from "./DTOMapper";

@allowStaticMethods<DTOMapper<Money, MoneyDTO>>()
export class MoneyMapper {
    static fromEntityToDTO(entity: Money): MoneyDTO {
        return {
            amount: entity.getAmount,
            currency: entity.getCurrency.getCurrencyCode
        }
    }
}