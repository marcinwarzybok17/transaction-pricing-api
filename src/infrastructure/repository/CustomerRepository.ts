import { Connection, getManager } from "typeorm";
import { DatabaseConnectionPooling } from "../../application/databases/DatabaseConnectionPooling";
import { DATABASES } from "../../application/databases/databases";
import { PostgresDatabaseRepository } from "./PostgresDatabaseRepository";

export interface ICustomerRepository extends PostgresDatabaseRepository {
    getTransactionSumForCustomerInLast30Days(customerId: number): Promise<number>
}

export class CustomerRepository implements ICustomerRepository {
    databaseConnection: Connection

    constructor() {
        this.databaseConnection = DatabaseConnectionPooling.getConnection(DATABASES.TRANSACTIONS)
    }

    async getTransactionSumForCustomerInLast30Days(customerId: number): Promise<number> {
        const result = await getManager().query(`
            SELECT SUM(amount)
            FROM transaction
            WHERE "transactionDate" > CURRENT_DATE - 30 AND "customerId" = $1
        `, [customerId])

        return result
    }


}