import { Currency } from "../../domain/value-object/Currency";
import { TransactionDate } from "../../domain/value-object/TransactionDate";

export type Rates = {
    [key in string]: number
}

export interface ExchangeApiRepository {
    getRatesForCurrency(currency: Currency, date: TransactionDate): Promise<Rates>
}