import { Connection } from "typeorm";

export interface PostgresDatabaseRepository {
    databaseConnection: Connection
}