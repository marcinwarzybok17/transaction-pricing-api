import { Currency } from "../../domain/value-object/Currency";
import { TransactionDate } from "../../domain/value-object/TransactionDate";
import { ExchangeApiRepository, Rates } from "./ExchangeApiRepository";
import axios from 'axios'
import { InfrastructureError, INFRASTRUCTURE_ERROR } from "../errors/InfrastructureError";

interface ExchangeRateApiRepositoryResponse {
    rates: Rates
}

export class ExchangeRateApiRepository implements ExchangeApiRepository {
    async getRatesForCurrency(currency: Currency, date: TransactionDate): Promise<Rates> {
        if (currency.getCurrencyCode !== 'EUR') {
            throw new Error(`Repository don't provide exchange rates for other currencies than euros`)
        }

        try {
            const request = await axios.request<ExchangeRateApiRepositoryResponse>({
                method: 'GET',
                url: `https://api.exchangerate.host/${date.getValue}`
            })

            return request.data.rates
        } catch (err) {
            console.log(err)
            throw new InfrastructureError(INFRASTRUCTURE_ERROR.INTERNAL_ERROR, ``)
        }

    }
}