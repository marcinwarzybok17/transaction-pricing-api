export interface TransactionPricingRule {
    apply(amount: number, ...args): number
}