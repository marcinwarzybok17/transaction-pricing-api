import { DefaultPricingRule } from "./DefaultPricingRule";
import { TransactionPricingRule } from "./TransactionPricingRule";

export class ClientWithDiscountRule implements TransactionPricingRule {

    getCustomersWithDiscounts(): number[] {
        return [42]
    }

    hasCustomerDiscount(client_id: number): boolean {
        return this.getCustomersWithDiscounts().includes(client_id)
    }

    apply(amount: number, client_id: number): number {
        if (this.hasCustomerDiscount(client_id)) {
            return 0.05
        }

        return new DefaultPricingRule().apply(amount)
    }

}