import { Currency } from "../../domain/value-object/Currency";
import { TransactionDate } from "../../domain/value-object/TransactionDate";
import { InfrastructureError, INFRASTRUCTURE_ERROR } from "../errors/InfrastructureError";
import { ExchangeApiRepository } from "../repository/ExchangeApiRepository";

export interface IExchangeService {
    exchangeFromCurrencyToEUR(transactionDate: TransactionDate, transactionCurrency: Currency, amount: number): Promise<number>
}

export class ExchangeService implements IExchangeService {
    constructor(private exchangeApiRepository: ExchangeApiRepository) { }

    async exchangeFromCurrencyToEUR(transactionDate: TransactionDate, transactionCurrency: Currency, amount: number): Promise<number> {
        const rates = await this.exchangeApiRepository.getRatesForCurrency(
            new Currency('EUR'),
            transactionDate
        )

        const rate = rates[transactionCurrency.getCurrencyCode]

        if (!rate) {
            throw new InfrastructureError(INFRASTRUCTURE_ERROR.VALIDATION_ERROR, `There is no rate for this currency code`)
        }

        const exchangedAmount = rate * amount

        return exchangedAmount
    }

}