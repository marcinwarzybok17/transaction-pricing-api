import { ClientWithDiscountRule } from "./ClientWithDiscountRule";
import { TransactionPricingRule } from "./TransactionPricingRule";

export class HighTurnOverDiscountRule implements TransactionPricingRule {
    isCustomerHasHighTurnoverDiscount(sumTransactionTurnOver: number): boolean {
        return sumTransactionTurnOver > 1000
    }

    apply(amount: number, customerId: number, sumTransactionTurnOver: number): number {
        const isCustomerHasHighTurnoverDiscount = this.isCustomerHasHighTurnoverDiscount(sumTransactionTurnOver)

        if (!isCustomerHasHighTurnoverDiscount) {
            return new ClientWithDiscountRule().apply(amount, customerId)
        }

        return 0.03
    }

}