import { Currency } from "../../domain/value-object/Currency";
import { Money } from "../../domain/value-object/Money";
import { TransactionDate } from "../../domain/value-object/TransactionDate";
import { MoneyDTO } from "../dto/Money";
import { MoneyMapper } from "../mappers/MoneyMapper";
import { ICustomerRepository } from "../repository/CustomerRepository";
import { IExchangeService } from "./ExchangeService";
import { TransactionPricingRule } from "./TransactionPricingRule";

export interface ITrasactionPricingService {
    getPricingForTransaction(transactionDate: string, transactionCurrency: string, amount: number, customerId: number): Promise<MoneyDTO>
}

export class TrasactionPricingService implements ITrasactionPricingService {
    constructor(private exchangeService: IExchangeService, private customerRepostiory: ICustomerRepository, private transactionPricingRule: TransactionPricingRule) { }

    async getPricingForTransaction(transactionDate: string, transactionCurrency: string, amount: number, customerId: number): Promise<MoneyDTO> {
        const amountInEur = await this.exchangeService.exchangeFromCurrencyToEUR(
            new TransactionDate(transactionDate),
            new Currency(transactionCurrency),
            amount
        )

        const sumTransactionTurnOver = await this.customerRepostiory.getTransactionSumForCustomerInLast30Days(customerId)

        const fee = this.transactionPricingRule.apply(amountInEur, customerId, sumTransactionTurnOver)
        const money = new Money(
            new Currency("EUR"),
            fee
        )

        return MoneyMapper.fromEntityToDTO(money)
    }
}