import { TransactionPricingRule } from "./TransactionPricingRule";

export const MIN_TRANSACTION_FEE = 0.05

export class DefaultPricingRule implements TransactionPricingRule {

    isCalculatedFeeSmallerThanMinPriceTransaction(calculatedFee: number): boolean {
        return calculatedFee < MIN_TRANSACTION_FEE
    }

    apply(amount: number): number {
        const calculatedFee = amount * 0.005

        if (this.isCalculatedFeeSmallerThanMinPriceTransaction(calculatedFee)) {
            return MIN_TRANSACTION_FEE
        }

        return calculatedFee
    }
}