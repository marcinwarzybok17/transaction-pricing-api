FROM node:14 as develop

WORKDIR /home/node/app

COPY package*.json ./

RUN yarn

COPY . .

ENTRYPOINT ["npm","run","dev"]